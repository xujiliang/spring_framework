package test.yuanma;

import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;

/**
 * @auther xjl
 * @date 2020/6/5
 * @description
 **/
public class Person implements InitializingBean {

	private String name;

	private String sex;


	@PostConstruct
	public void init() {
		System.out.println("哈哈哈哈哈");
	}
	@Override
	public void afterPropertiesSet() throws Exception {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
}
