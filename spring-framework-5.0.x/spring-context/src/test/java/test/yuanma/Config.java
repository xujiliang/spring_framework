package test.yuanma;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @auther xjl
 * @date 2020/6/5
 * @description
 **/
@Configuration
public class Config {

	@Bean
	public Person person() {
		return new Person();
	}
}
