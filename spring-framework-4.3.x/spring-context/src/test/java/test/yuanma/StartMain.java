package test.yuanma;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import test.yuanma.service.MessageService;

/**
 * @auther xjl
 * @date 2020/6/5
 * @description
 **/
public class StartMain {

	public static void main(String[] args) {
//		AnnotationConfigApplicationContext acac = new AnnotationConfigApplicationContext(Config.class);
//
//		Person person = (Person) acac.getBean("person");
//
//		System.out.println(person.getSex());
		System.out.println("applicationContext启动起来了");

		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationfile.xml");

		MessageService messageService = applicationContext.getBean(MessageService.class);
		System.out.println(messageService.getMessage());
	}
}
